FROM python:3.9.1-alpine

RUN apk add --update --no-cache \
    g++ gcc libxslt-dev musl-dev python3-dev \
    libffi-dev openssl-dev jpeg-dev zlib-dev


ENV LIBRARY_PATH=/lib:/usr/lib


# add and change to non-root user in image
ENV HOME=/home/worker
RUN adduser --home ${HOME} --disabled-password worker
RUN chown worker:worker ${HOME}
USER worker

# Set the working directory for any following RUN, CMD, ENTRYPOINT, COPY and ADD instructions
WORKDIR ${HOME}


COPY ./requirements.txt ${HOME}

RUN pip install --upgrade pip

RUN pip install --no-cache-dir -r requirements.txt

COPY . ${HOME}
